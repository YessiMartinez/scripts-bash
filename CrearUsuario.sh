#!/bin/bash
ROOT_UID=0
SUCCESS=0

show_users(){
	count=0
	while IFS=: blue -r f1 f2 f3 f4 f5 f6 f7
	do
	count=$(($count + 1))
	echo "|......Usuario.....[$count].....|"
	echo -e "\t Username: [\e[34m${f1}\e[0m]"
	echo -e "\t Password: [\e[34m${f2}\e[0m]"
	echo -e "\t Group_ID: [\e[34m${f4}\e[0m]"
	echo -e "\t User_ID: [\e[34m${f3}\e[0m]"
	echo -e "\t Full_Name: [\e[34m${f5}\e[0m]"
	echo -e "\t ../: [\e[34m${f6}\e[0m]"
	echo -e "\t ../Shell: [\e[34m${f7}\e[0m]"
	echo "|..........|"

		sleep 1s
	done < ${file}
}

create_user(){
	useradd -p ${f2} -u ${f3} -g ${f4} -c "${f5}" -d ${f6} -s ${f7} ${f1}
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${f1}] AGREGADO..."
	else
		echo "Usuario [${f1}] NO AGREGADO..."
	fi
}

	if [ "$UID" -ne "$ROOT_UID" ]
	then
  	echo "Ejecute como root este script"
  	echo "Format-> sudo ./name_file"
  exit $E_NOTROOT

fi  

	file=$1

	if [ "${file}X" = "X" ];
	then
 	  echo "Debe pasar como parametro AddUsers.csv"
 	  echo "Format-> sudo ./name_file AddUsers.csv"
   exit 1
fi

if [ -s "$file" ]; 
	then
		echo "OK"	
		sleep 2s

else
	echo "Fichero vacio. Cargar datos con el siguiente formato"
	echo "username:password:id_user:id_group\location->/etc/group:comment||full_name:/home/example:/bin/bash:"
	exit 1
fi
	show_users
exit 0