#!/bin/bash
ROOT_UID=0
SUCCESS=0

	if [ "$UID" -ne "$ROOT_UID" ]
	then
	  echo "Ejecute como root este script"
exit $E_NOTROOT

	fi  

	file=$1

	if [ "${file}X" = "X" ];
	then
	   echo "Debe pasar como parametro el archivo donde estan los grupos"
exit 1

fi


eliminarGrupo(){
	#echo "ELIMINAR USUARIO"
	
	#echo "Username 		  = ${user}"
	#echo "******************************"

	groupdel "${user}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${user}] ELIMINADO CORRECTAMENTE..."
	else
		echo "Usuario [${user}] NO SE HA ELIMINADO..."
	fi
}

while IFS=: read -r f1
do
	eliminarGrupo "\${f1}"	
done < ${file}

exit 0